

## Структура проекта

#### 

 **- src** — рабочая папка, где хранятся исходники
 
**- dist** — оптимизированные файлы

## Сборка проекта tasks:

Устанавливаем пакеты:

    npm i  

Проверяем структуру тасков:

    gulp --tasks

Сборка проекта в **/dist/**:

    gulp build  

Запуск реверсной сборки проекта с отслеживанием изменений в кастомных файлах:
     
    gulp dev

    
Ссылка на сайт https://serhii2014.github.io/OnePartners/